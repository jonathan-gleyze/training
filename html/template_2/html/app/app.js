(function($){
    var $debug = false; /*config*/
    
    var $open = false;
    var $windowWidth = $(window).width();

    if($debug === true){console.log(
        'succes : jquery loaded \n' +
        'screen width : ' + $windowWidth + '\n' +
        'nav open : ' + $open
    );}
    
    
    
    $("#icon_toggle").click( function() {
        if($debug === true){console.log('succes :click icon')}      
        $('nav ul').toggleClass('hide');
        $('.text-grap').toggleClass('text-grap-top');
        open = !(open);
    }); 
    
    /*fix resize*/
    $(window).resize(function() {
        if($windowWidth != $(window).width()){
            location.reload();
            if($debug === true){console.log('succes :reload')} 
            return;
        }
    });
    
    if ($windowWidth > '1000') {    
        $('nav ul').removeClass('hide');
        if($debug === true){console.log('succes : $windowWidth > 1000 remove Class(hide)')}
    };
    
})(jQuery);