(function($){  
    var element = "div[data-plugins='btn-nav-res']"
    var theme = $(element).attr("data-theme");    
    var direction = $(element).attr("date-direction");

    /* init element*/
    $(element).addClass('nav-icon');   
    $( "<span></span>" ).appendTo($(element));
    
    /* add action clcik action change style */
    $(element).click( function() {
        switch(theme) {
            case "cross":
                $(element).toggleClass("nav-icon-is-opened-cross");
                break;
            case "arrow":
                AutoDir(direction);
                break;
            default:
                $(element).toggleClass("nav-icon-is-opened-cross");
                break;
        };
    });
    
    /* date-direction is not defined then $direct */ 
    function AutoDir(direct) {
        if(direction = "undefined"){
            direct = 'right';
        }
        $(element).toggleClass('nav-icon-is-opened-arrow-'+direct);
    }
})(jQuery);